//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>

float fetch(){
    float a;
    scanf("%f", &a);
    return a;
}

float distance(float x1, float y1, float x2, float y2){
    float dist = sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    return dist;
}

void printDist(float a){
    printf("The distance between the two points is %f", a);
}

int main(){
    printf("Enter the x coordinate of the 1st pt.: ");
    float x1 = fetch();
    
    printf("Enter the y coordinate of the 1st pt.: ");
    float y1 = fetch();
     
    printf("Enter the x coordinate of the 2nd pt.: ");
    float x2 = fetch();
    
    printf("Enter the y coordinate of the 2nd pt.: ");
    float y2 = fetch();
    
    float dist = distance(x1,y1,x2,y2);
    
    printDist(dist);
    
    return 0;
}