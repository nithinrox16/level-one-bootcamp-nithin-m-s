//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>

float input()
{
    float a;
    scanf("%f", &a);
    return a;
}

float volume(float h, float d, float b)
{
    float vol = (1.0/3.0)*((h*d)+d)/b;
    return vol;
}

float output(float a)
{
    printf("The volume of the tromboloid is %f", a);
    return a;
}

void main()
{
    float h, d, b, vol;
    
    printf("Enter h: ");
    h = input();
    
    printf("Enter d: ");
    d = input();
    
    printf("Enter b: ");
    b = input();
    
    vol = volume(h,d,b);
    
    output(vol);
}